(ns gema.utils
  (:require [clojure.string :as str]
            #?(:clj  [clj-http.lite.client :as client]
               :cljs [cljs-http.client :as client])
            #?(:clj  [clojure.core :refer [char-array read-string]]
               :cljs [cljs.pprint :refer [char-code]])
            #?(:cljs [cljs.reader :refer [read-string]])))


(def URL-BASE "https://www.urbanomic.com/glossget.php?status=idle&system=AQ&wait=0&glossary=&result1=")

(defn filter-val [in-str in-reg in-split]
  "Filter some arbitrary regexes from a vector"
  (filterv (fn [x] (re-find in-reg x)) (str/split in-str in-split)))

(defn aq-conv [in-str]
  "Turn string into AQ values"
  (let [num-index #?(:clj  (fn [x] (mapv int
                                         (char-array x)))
                     :cljs (fn [x] (mapv char-code
                                         (vec x))))]
    (mapv (fn [x] (- x 87))
          (num-index in-str))))

(defn cipher [in-str]
  "Get the AQ value of a word + numbers if present"
  (let [in-nums (mapv read-string
                      (filter-val in-str
                                  #"[0-9]" #""))
        in-letters (str/join (filter-val (str/lower-case in-str)
                                         #"[a-z]" #""))]
    (reduce + (into [] (concat in-nums
                               (aq-conv in-letters))))))

(defn call-site [in-fn]
  "Make a generic call to the Gematrix"
  (mapv (fn [x] (str/replace x #".*(\=)" "= "))
        (filter-val
         (get (client/get in-fn) :body)
         #"^(\d+)(=.+)$"
         #"\n")))

(defn get-eq [in-str]
  "Get the list of equivalent values from the Urbanomic gematrix"
  (let [aq-val (cipher in-str)]
    (call-site (str URL-BASE
                    aq-val))))

(defn get-rev [{:keys [in-val]}]
  "Lookup the AQ value itself"
  (let [aq-map (call-site (str URL-BASE
                               in-val))]
    (println (str in-val
                  "\n"
                  (str/join "\n"
                            aq-map)))))

(defn pretty-aq [{:keys [in-str]}]
  "Print everything out in a nice looking format"
  (let [aq-map (get-eq in-str)
        aq-val (cipher in-str)]
    (println (str in-str
                  " = "
                  aq-val
                  "\n"
                  (str/join "\n"
                            aq-map)))))
