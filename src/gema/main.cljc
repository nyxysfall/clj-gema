(ns gema.main
  (:require [clj-http.lite.client :as client]
            [cli-matic.core :refer [run-cmd]]
            [clojure.string :as str]
            [gema.utils :refer [pretty-aq get-rev]])
  (:gen-class))

(def CONFIGURATION
  {:command "gema"
   :description "A command line gematriculator"
   :version "0.0.1"
   :opts []
   :subcommands [{:command "cipher"
                  :description "Cipher a string"
                  :opts [{:as "input string"
                          :option "in-str"
                          :short 0
                          :type :string}]
                  :runs pretty-aq}
                 {:command "reverse"
                  :description "Lookup an AQ value"
                  :opts [{:as "input value"
                          :option "in-val"
                          :short 0
                          :type :int}]
                  :runs get-rev}]})

(defn -main [& args]
  (run-cmd args CONFIGURATION))
