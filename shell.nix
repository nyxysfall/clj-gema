{ pkgs ? import <nixpkgs> {} }:

with pkgs;

let
  graalvm = pkgs.callPackage ./graalvm.nix {};
in

mkShell {
  buildInputs = [
    graalvm.graalvm11-ce
    pkgs.musl
    pkgs.zlib
    pkgs.stdenv
  ];
}
